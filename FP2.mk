#TARGET_DISABLE_DASH := true
#TARGET_DISABLE_OMX_SECURE_TEST_APP := true

# Inherit device configurations
$(call inherit-product, device/fairphone_devices/FP2/device.mk)

# Device identifications
PRODUCT_NAME := FP2
PRODUCT_DEVICE := FP2
PRODUCT_BRAND := Fairphone
PRODUCT_MODEL := FP2
PRODUCT_MANUFACTURER := Fairphone

# Display logo image file
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/brand/splash.img:$(PRODUCT_OUT)/splash.img

#battery_monitor
PRODUCT_PACKAGES += \
    battery_monitor \
    battery_shutdown

# Radio releasetools
TARGET_RELEASETOOLS_EXTENSIONS := device/fairphone_devices/FP2
ADD_RADIO_FILES := true

# Enable strict operation
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.strict_op_enable=false \
    persist.sys.usb.config=mtp

PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.whitelist=/system/etc/whitelist_appops.xml

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/appops/whitelist_appops.xml:system/etc/whitelist_appops.xml

# Fairphone OS packages
PRODUCT_PACKAGES += \
    FairphoneUpdater \
    FairphoneLauncher3 \
    AppOps \
    MyContactsWidget \
    ClockWidget \
    FairphonePrivacyImpact \
    FairphoneSetupWizard \
    ProgrammableButton \
    ProximitySensorTools \
    Checkup

# iFixit Repair Manuals
PRODUCT_PACKAGES += iFixit

# Amaze File Manager
PRODUCT_PACKAGES += Amaze

# Add boot animation
PRODUCT_COPY_FILES += $(LOCAL_PATH)/brand/bootanimation.zip:system/media/bootanimation.zip

# Set default ringtone to Fairphone's
PRODUCT_COPY_FILES += $(LOCAL_PATH)/brand/Sunbeam.mp3:system/media/audio/ringtones/Fairphone.mp3
PRODUCT_COPY_FILES += $(LOCAL_PATH)/brand/Fiesta.mp3:system/media/audio/ringtones/Fiesta.mp3

# Preferred Applications for Fairphone
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/brand/preferred.xml:system/etc/preferred-apps/fp.xml

ifeq ($(strip $(FP2_SKIP_BOOT_JARS_CHECK)),)
SKIP_BOOT_JARS_CHECK := true
endif

# SuperUser
FP2_USE_APPOPS_SU := true
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.root_access=0

# we don't have the calibration data so don't generate persist.img
FP2_SKIP_PERSIST_IMG := true
