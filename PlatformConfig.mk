# Platform
TARGET_BOARD_PLATFORM := msm8974
TARGET_CPU_SMP := true

# Architecture
TARGET_ARCH := arm
TARGET_KERNEL_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_ABI  := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_VARIANT := krait
ARCH_ARM_HAVE_TLS_REGISTER := true

# C/C++ flags
TARGET_GLOBAL_CFLAGS += -mfpu=neon -mfloat-abi=softfp
TARGET_GLOBAL_CPPFLAGS += -mfpu=neon -mfloat-abi=softfp

# QCOM BSP
TARGET_USES_QCOM_BSP := true
