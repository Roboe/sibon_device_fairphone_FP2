# Device specific overlays
DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay

# Product common configurations
$(call inherit-product, device/qcom/common/common.mk)

# Device product elements
include $(LOCAL_PATH)/product/*.mk
