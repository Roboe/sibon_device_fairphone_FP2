# Bootloader
TARGET_BOOTLOADER_BOARD_NAME := FP2

# Images
TARGET_NO_BOOTLOADER := false
TARGET_NO_KERNEL := false
TARGET_NO_RADIOIMAGE := true
TARGET_NO_RPC := true

# Use signed boot and recovery image
TARGET_BOOTIMG_SIGNED := true
