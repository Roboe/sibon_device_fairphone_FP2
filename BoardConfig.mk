# Board device path
DEVICE_PATH := device/fairphone_devices/FP2

# Board device headers
TARGET_SPECIFIC_HEADER_PATH := $(DEVICE_PATH)/include

# Board device elements
include $(DEVICE_PATH)/PlatformConfig.mk
include $(DEVICE_PATH)/board/*.mk

# Board device vendor
-include $(QCPATH)/common/msm8974/BoardConfigVendor.mk
