# media_profiles and media_codecs xmls for 8974
ifeq ($(TARGET_ENABLE_QC_AV_ENHANCEMENTS), true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/media/media_profiles_8974.xml:system/etc/media_profiles.xml \
    $(LOCAL_PATH)/media/media_codecs_8974.xml:system/etc/media_codecs.xml
endif
