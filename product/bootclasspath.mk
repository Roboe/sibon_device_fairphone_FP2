# Boot classpath
PRODUCT_BOOT_JARS += \
    qcmediaplayer \
    org.codeaurora.Performance \
    vcard \
    tcmiface

ifneq ($(strip $(QCPATH)),)
PRODUCT_BOOT_JARS += \
    WfdCommon \
    qcom.fmradio \
    security-bridge \
    qsb-port \
    oem-services
endif

