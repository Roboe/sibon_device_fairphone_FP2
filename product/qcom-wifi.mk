# WiFi WCNSS configurations
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/wifi/WCNSS_qcom_cfg.ini:system/etc/wifi/WCNSS_qcom_cfg.ini \
    $(LOCAL_PATH)/wifi/WCNSS_qcom_wlan_nv.bin:persist/WCNSS_qcom_wlan_nv.bin

# WiFi packages
PRODUCT_PACKAGES += \
    wcnss_service \
    wpa_supplicant_overlay.conf \
    p2p_supplicant_overlay.conf
